package tabloo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        loadMainBorderPaneFXML(primaryStage);
        configureMainWindow(primaryStage);
        primaryStage.show();
    }

    private void configureMainWindow(Stage primaryStage) {
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.setFullScreen(true);
    }

    private void loadMainBorderPaneFXML(Stage primaryStage) throws java.io.IOException {
        FXMLLoader mainBorderPaneFxmlLoader = new FXMLLoader(this.getClass().getResource("GUI/fxml/MainPane.fxml"));
        BorderPane mainBorderPane = mainBorderPaneFxmlLoader.load();
        Scene scene = new Scene(mainBorderPane);
        scene.getStylesheets().add(this.getClass().getResource("GUI/fxml/kappa.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(Main.class.getResource("GUI/images/zsp2_icon.png").toExternalForm()));
    }
}