package tabloo.GUI.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import tabloo.Main;
import tabloo.searchEngine.config.ConfigSetter;
import tabloo.searchEngine.config.validators.ValidationChecker;
import tabloo.searchEngine.result.Result;

import java.io.IOException;

/**
 * Created by getor on 25.04.17.
 */
public class MainPaneController {
    private HeaderController headerController;
    private Result[] results;
    @FXML private GridPane header;
    @FXML private StackPane mainPaneStackPane;

    @FXML public void initialize() {
        try {
            loadHeader();
            loadHomeScreen();
            readConfig();
            validateConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadHeader() throws IOException {
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("GUI/fxml/Header.fxml"));
        GridPane header = fxmlLoader.load();
        headerController = fxmlLoader.getController();
        headerController.setMainPaneController(this);
        this.header.getChildren().add(header);
        header.maxWidthProperty().bind(this.header.maxWidthProperty());
    }

    public void loadHomeScreen() {
        createNewElementToStackPane("HomeScreen.fxml");
    }

    private void loadResultsListView() {
        headerController.disableArrowButton(true);
        ResultListPaneController resultListPaneController =
                createNewElementToStackPane("ResultsListPane.fxml");
        resultListPaneController.setMainPaneController(this);
        resultListPaneController.displayResults(results);
        resultListPaneController.setMainPaneController(this);
    }

    public void informAboutNoResults() {
        createNewElementToStackPane("NoResults.fxml");
    }

    public void informAboutIncorrectNumberOfInformation() {
        createNewElementToStackPane("IncorrectArgumentNumber.fxml");
    }

    private void informAboutConfigOrConnectionProblem() {
        createNewElementToStackPane("ConfigProblem.fxml");
    }

    public void loadImageView(int indexOfPressedButton) {
        headerController.disableArrowButton(false);
        ImageViewPaneController imageViewPaneController =
                createNewElementToStackPane("ImageViewPane.fxml");
        imageViewPaneController
                .setPathToImage(results[indexOfPressedButton].urlToFile);
        imageViewPaneController.showImage();
    }

    private <T> T createNewElementToStackPane(String fileName) {
        mainPaneStackPane.getChildren().clear();
        FXMLLoader fxmlLoader =
                new FXMLLoader(Main.class.getResource("GUI/fxml/" + fileName));
        StackPane stackPane = loadFXML(fxmlLoader);
         mainPaneStackPane.getChildren().add(stackPane);
         setCorrectSize(stackPane);
        return fxmlLoader.getController();
    }

    private <T> T loadFXML(FXMLLoader fxmlLoader) {
        try {
            return fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setCorrectSize(StackPane view) {
        view.maxWidthProperty().bind(mainPaneStackPane.maxWidthProperty());
        view.maxHeightProperty().bind(mainPaneStackPane.maxHeightProperty());
    }

    private void readConfig() {
        ConfigSetter configSetter = new ConfigSetter();
        configSetter.setConfigFromConfigFiles();
    }

    private void validateConfig() throws IOException {
        ValidationChecker validationChecker = new ValidationChecker();
        if(!validationChecker.testConfigFiles()) {
            informAboutConfigOrConnectionProblem();
            disableHeader();
        }
    }

    private void disableHeader() {
        header.setDisable(true);
    }

    public void handleResults() {
        if(results.length == 0)
            informAboutNoResults();
        else
            loadResultsListView();
    }

    public void setResults(Result[] results) {
        this.results = results;
    }

}
