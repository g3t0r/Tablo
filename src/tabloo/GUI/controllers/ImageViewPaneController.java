package tabloo.GUI.controllers;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 * Created by getor on 29.04.17.
 */
public class ImageViewPaneController {

    private String pathToImage;
    @FXML private StackPane stackPane;
    @FXML private ImageView imageViewForPhoto;

    public void showImage() {
        imageViewForPhoto.setImage(new Image("file:" + pathToImage));
        scaleImageToSizeOfPane();
    }

    private void scaleImageToSizeOfPane() {
        imageViewForPhoto.fitHeightProperty().bind(stackPane.heightProperty());
        imageViewForPhoto.fitWidthProperty().bind(stackPane.widthProperty());
    }

    public void setPathToImage(String pathToImage) {
        this.pathToImage = pathToImage;
    }
}
