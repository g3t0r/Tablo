package tabloo.GUI.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

/**
 * Created by getor on 05.05.17.
 */
public class ConfigProblemController {
    @FXML private StackPane stackPane;
    @FXML private Label polishLabel;
    @FXML private Label englishLabel;

    @FXML public void initialize() {
        stackPane.widthProperty().addListener(event -> {
            polishLabel.setFont(Font.font(stackPane.getWidth() / 30));
            englishLabel.setFont(Font.font(stackPane.getWidth() / 50));
        });
    }
}
