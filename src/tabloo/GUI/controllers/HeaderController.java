package tabloo.GUI.controllers;

import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import tabloo.searchEngine.SearchEngine;
import tabloo.searchEngine.result.Result;

/**
 * Created by getor on 04.05.17.
 */
public class HeaderController {
    private MainPaneController mainPaneController;
    @FXML private Button arrowButton;
    @FXML private JFXTextField nameField;
    @FXML private JFXTextField surnameField;
    @FXML private JFXTextField specializationField;
    @FXML private JFXTextField yearField;

    private String name;
    private String surname;
    private String specialization;
    private String year;
    private Result[] results;

    @FXML public void initialize() {
        arrowButton.setDisable(true);
    }

    @FXML void arrowButtonPressed() {
        mainPaneController.handleResults();
    }

    @FXML public void searchButtonPressed() {
        searchForResults();
    }

    @FXML public void onEnterPressed() {
        searchForResults();
    }

    @FXML public void homeButtonPressed() {
        mainPaneController.loadHomeScreen();
    }

    public void disableArrowButton(boolean trueOfFalse) {
        arrowButton.setDisable(trueOfFalse);
    }

    private void searchForResults() {

        if(textFieldsAreNotEmpty()) {
            name = setText(nameField);
            surname = setText(surnameField);
            specialization = setText(specializationField);
            year = setText(yearField);


            SearchEngine searchEngine = new SearchEngine(new String[]{name, surname, specialization, year});
            results = searchEngine.searchForResults();
            mainPaneController.setResults(results);
            mainPaneController.handleResults();
        }
    }


    private void showIncorrectNumberOfInformationScreen() {
        mainPaneController.informAboutIncorrectNumberOfInformation();
    }

    public void setMainPaneController(MainPaneController mainPaneControllerA) {
        mainPaneController = mainPaneControllerA;
    }

    private String setText(JFXTextField textField) {
        if(textField.getText().isEmpty()) return "";
        return textField.getText();
    }


    private boolean textFieldsAreNotEmpty() {
        return !(nameField.getText().isEmpty() && surnameField.getText().isEmpty() &&
        specializationField.getText().isEmpty() &&  yearField.getText().isEmpty());
    }
}
