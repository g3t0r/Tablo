package tabloo.GUI.controllers;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import tabloo.Main;
import tabloo.searchEngine.result.Result;

import java.io.IOException;

/**
 * Created by getor on 30.04.17.
 */
public class ResultListPaneController {
    private MainPaneController mainPaneController;
    private Result[] results;
    @FXML private VBox vbox;


    public void displayResults(Result[] results) {
        setResults(results);
        addButtonsToPane(results.length);
    }

    private void setResults(Result[] results) {
        this.results = results;
    }

    private void addButtonsToPane(int numberOfResults) {
        for(int i = 0; i < numberOfResults; i++) {
            JFXButton button = new JFXButton();
            ButtonController[] buttonControllers = new ButtonController[numberOfResults];

            try {
                addButtonsWithControllers(button, buttonControllers, i);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addButtonsWithControllers(Button button, ButtonController[] buttonControllers, int i) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("GUI/fxml/Button.fxml"));
        button = fxmlLoader.load();
        createAndSetButtonController(i, buttonControllers, fxmlLoader);
        addButtonToResultListPane(i, button);
        setCorrectWidthOfButton(button);
    }

    private void setCorrectWidthOfButton(Button button) {
        button.maxWidthProperty().bind(vbox.maxWidthProperty());
    }

    private void addButtonToResultListPane(int i, Button button) {
        vbox.getChildren().add(button);
        button.setText(results[i].toStringForButtons());
    }

    private void createAndSetButtonController(int i, ButtonController[] buttonControllers, FXMLLoader fxmlLoader) {
        buttonControllers[i] = fxmlLoader.getController();
        buttonControllers[i].setButtonIndex(i);
        buttonControllers[i].setMainPaneController(mainPaneController);
    }

    public void setMainPaneController(MainPaneController mainPaneController) {
        this.mainPaneController = mainPaneController;
    }

}
