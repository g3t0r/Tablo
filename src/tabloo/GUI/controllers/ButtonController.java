package tabloo.GUI.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

/**
 * Created by getor on 15.05.17.
 */
public class ButtonController {
    private int buttonIndex;
    private MainPaneController mainPaneController;
    @FXML public Button button;

    @FXML public void buttonPressed() {
        mainPaneController.loadImageView(buttonIndex);
    }

    public void setButtonIndex(int buttonIndex) {
        this.buttonIndex = buttonIndex;
    }

    public void setMainPaneController(MainPaneController mainPaneController) {
        this.mainPaneController = mainPaneController;
    }
}
