package tabloo.searchEngine.filtr;

import tabloo.searchEngine.result.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by getor on 09.07.17.
 */
public class SearchFilterBySpecialization implements SearchFilter{
    private ArrayList<Result> unfilteredArray;
    private String specialization;

    public SearchFilterBySpecialization(String specialization, Result[] results) {
        unfilteredArray = new ArrayList<>(Arrays.asList(results));
        this.specialization = specialization;
    }

    @Override
    public Result[] getFilteredResults() {
        Iterator<Result> iterator = unfilteredArray.iterator();
        while (iterator.hasNext()) {
            Result r = iterator.next();
            if(!isSpecializationCorrect(r, specialization)) iterator.remove();
        }
        return convertArrayListToArray(unfilteredArray);
    }
}
