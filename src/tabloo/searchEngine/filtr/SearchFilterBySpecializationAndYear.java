package tabloo.searchEngine.filtr;

import tabloo.searchEngine.result.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by getor on 09.07.17.
 */
public class SearchFilterBySpecializationAndYear implements SearchFilter{
    private ArrayList<Result> unfilteredArray;
    private String year;
    private String specialization;

    public SearchFilterBySpecializationAndYear(String year, String specialization, Result[] results) {
        unfilteredArray = new ArrayList<>(Arrays.asList(results));
        this.year = year;
        this.specialization = specialization;
    }

    @Override
    public Result[] getFilteredResults() {
        Iterator<Result> iterator = unfilteredArray.iterator();
        while (iterator.hasNext()) {
            Result result = iterator.next();
            if(!(isYearCorrect(result, year) && isSpecializationCorrect(result, specialization))) iterator.remove();
        }
        return convertArrayListToArray(unfilteredArray);
    }
}
