package tabloo.searchEngine.filtr;

import tabloo.searchEngine.result.Result;

import java.util.ArrayList;

/**
 * Created by getor on 09.07.17.
 */
public interface SearchFilter {
    public Result[] getFilteredResults();

    default Result[] convertArrayListToArray(ArrayList<Result> arrayList) {
        Result[] array = new Result[arrayList.size()];
        array = arrayList.toArray(array);
        return array;
    }

    default boolean isYearCorrect(Result result, String year) {
        return result.yearOfSchoolCompletion == Integer.parseInt(year);
    }

    default boolean isSpecializationCorrect(Result result, String specialization) {
        return result.specialization.toLowerCase().contains(specialization.toLowerCase());
    }
}
