package tabloo.searchEngine.filtr;

import tabloo.searchEngine.result.Result;

/**
 * Created by getor on 09.07.17.
 */
public class StudentResultFilter {
    private String specialization;
    private String year;
    private Result[] results;
    private SearchFilter searchFilter;

    public StudentResultFilter(String specialization, String year, Result[] results) {
        this.specialization = specialization;
        this.year = year;
        this.results = results;

    }

    public Result[] getFilteredArray() {

        if(!(specialization.isEmpty() && year.isEmpty())) {
            searchFilter = new SearchFilterBySpecializationAndYear(year, specialization, results);
        }
        if(specialization.isEmpty() && !year.isEmpty()) {
            searchFilter = new SearchFilterByYear(year, results);
        }
        if(!specialization.isEmpty() && year.isEmpty()) {
            searchFilter = new SearchFilterBySpecialization(specialization, results);
        }
        return searchFilter.getFilteredResults();
    }


}
