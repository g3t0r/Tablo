package tabloo.searchEngine.filtr;

import tabloo.searchEngine.result.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by getor on 09.07.17.
 */
public class SearchFilterByYear implements SearchFilter{
    private ArrayList<Result> unfilteredArray;
    private String year;

    public SearchFilterByYear(String year, Result[] results) {
        unfilteredArray = new ArrayList<>(Arrays.asList(results));
        this.year = year;
    }

    @Override
    public Result[] getFilteredResults() {
        Iterator<Result> iterator = unfilteredArray.iterator();
        while (iterator.hasNext()) {
            Result r = iterator.next();
            if(!isYearCorrect(r, year)) iterator.remove();
        }
        return convertArrayListToArray(unfilteredArray);
    }
}
