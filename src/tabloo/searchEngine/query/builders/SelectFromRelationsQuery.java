package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.RelationsTableConfig;

/**
 * Created by getor on 19.03.17.
 */
public class SelectFromRelationsQuery extends SelectQuery {
    private final String TABLE = RelationsTableConfig.TABLE;
    private final String STUDENT_ID_COLUMN_IN_TABLE = RelationsTableConfig.STUDENT_ID_COLUMN;
    private final String CLASS_ID_COLUMN_IN_TABLE = RelationsTableConfig.CLASS_ID_COLUMN;
    private int studentOrClassId;
    private int studentId;
    private int classId;

    public SelectFromRelationsQuery(int studentOrClassId) {
        this.query = createBasicSelectFromTable(TABLE);
        this.studentOrClassId = studentOrClassId;
    }

    public SelectFromRelationsQuery(int studentId, int classId) {
        this.query = createBasicSelectFromTable(TABLE);
        this.studentId = studentId;
        this.classId = classId;
    }

    public String createSelectByStudentId() {
        return query + "WHERE " + STUDENT_ID_COLUMN_IN_TABLE +
            "=" + addQuotationMarkToNumber(studentOrClassId);
    }

    public String createSelectByClassId() {
        return query + "WHERE " + CLASS_ID_COLUMN_IN_TABLE +
            "=" + addQuotationMarkToNumber(studentOrClassId);
    }

    public String createSelectByStudentIdAndClassId() {
        return query + "WHERE " + STUDENT_ID_COLUMN_IN_TABLE +
            "=" + addQuotationMarkToNumber(studentId) +
            " AND " + CLASS_ID_COLUMN_IN_TABLE +
            "=" + addQuotationMarkToNumber(classId);
    }
}
