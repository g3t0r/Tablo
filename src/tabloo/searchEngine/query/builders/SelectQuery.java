package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.ClassTableConfig;

/**
 * Created by getor on 18.03.17.
 */
public abstract class SelectQuery {
    protected final String ID_COLUMN_IN_TABLE = ClassTableConfig.ID_COLUMN;
    protected String TABLE;
    protected String query;
    protected int id;

    protected SelectQuery(int id) {
        this.id = id;
    }

    protected SelectQuery() {}

    public String createSelectById() {
        return query + "WHERE " + ID_COLUMN_IN_TABLE +
            "=" + addQuotationMarkToNumber(id);
    }

    protected String addQuotationMarkToString(String string) {
        return "\"" + string + "\"";
    }

    protected String addQuotationMarkToNumber(int number) {
        return "\"" + number + "\"";
    }

    protected String createBasicSelectFromTable(String TABLE) {
        return createBasicOfQuery() + TABLE + " ";
    }

    public String createBasicOfQuery() {
        return "SELECT * FROM ";
    }
}
