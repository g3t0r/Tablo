package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.StudentTableConfig;


public class StudentTestingQuery extends SelectQuery{
    private final String TABLE = StudentTableConfig.TABLE;
    private final String NAME_COLUMN_IN_TABLE = StudentTableConfig.NAME_COLUMN;
    private final String SURNAME_COLUMN_IN_TABLE = StudentTableConfig.SURNAME_COLUMN;

    public StudentTestingQuery() {
        this.query = createBasicSelectFromTable(TABLE);
    }

    public String createTestingQuery(){
        return query + "WHERE " + NAME_COLUMN_IN_TABLE + "="
            + addQuotationMarkToString("") + " AND "
            + SURNAME_COLUMN_IN_TABLE
            + "=" + addQuotationMarkToString("") + " AND "
            + ID_COLUMN_IN_TABLE + "=" + addQuotationMarkToNumber(0);
    }

}
