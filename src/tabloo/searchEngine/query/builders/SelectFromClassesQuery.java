package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.ClassTableConfig;

/**
 * Created by getor on 18.03.17.
 */
public class SelectFromClassesQuery extends SelectQuery {
    private final String TABLE = ClassTableConfig.TABLE;
    private final String SPECIALIZATION_COLUMN_IN_TABLE = ClassTableConfig.SPECIALIZATION_COLUMN;
    private final String YEAR_OF_SCHOOL_COMPLETION_COLUMN_IN_TABLE = ClassTableConfig.YEAR_OF_SCHOOL_COMPLETION_COLUMN;
    private String specialization;
    private int yearOfSchoolCompletion;

    public SelectFromClassesQuery(String specialization, int yearOfSchoolCompletion) {
        this.query = createBasicSelectFromTable(TABLE);
        this.specialization = specialization;
        this.yearOfSchoolCompletion = yearOfSchoolCompletion;
    }

    public SelectFromClassesQuery(int id) {
        super(id);
        this.query = createBasicSelectFromTable(TABLE);
    }

    public String createSelectBySpecializationAndYearOfSchoolCompletion() {
        return query + "WHERE " + createSelectBySpecializationPart() +
            " AND " + createSelectByYearOfSchoolCompletionPart();
    }

    public String createSelectBySpecialization() {
        return query + "WHERE " + createSelectBySpecializationPart();
    }

    public String createSelectByEndOfTheYear() {
        return query + "WHERE " + createSelectByYearOfSchoolCompletionPart();
    }

    private String createSelectBySpecializationPart() {
        return SPECIALIZATION_COLUMN_IN_TABLE + "=" + addQuotationMarkToString(specialization);
    }

    private String createSelectByYearOfSchoolCompletionPart() {
        return YEAR_OF_SCHOOL_COMPLETION_COLUMN_IN_TABLE + "=" + addQuotationMarkToNumber(yearOfSchoolCompletion);
    }
}
