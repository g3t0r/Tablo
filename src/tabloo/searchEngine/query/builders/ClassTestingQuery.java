package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.ClassTableConfig;

public class ClassTestingQuery extends SelectQuery {
    private final String TABLE = ClassTableConfig.TABLE;
    private final String SPECIALIZATION_COLUMN_IN_TABLE
        = ClassTableConfig.SPECIALIZATION_COLUMN;
    private final String YEAR_OF_SCHOOL_COMPLETION_COLUMN_IN_TABLE
        = ClassTableConfig.YEAR_OF_SCHOOL_COMPLETION_COLUMN;

    public ClassTestingQuery() {
        this.query = createBasicSelectFromTable(TABLE);
    }

    public String createTestingQuery() {
        return query + "WHERE " + SPECIALIZATION_COLUMN_IN_TABLE + "="
            + addQuotationMarkToString("") + " AND "
            + YEAR_OF_SCHOOL_COMPLETION_COLUMN_IN_TABLE
            + "=" + addQuotationMarkToString("") + " AND "
            + SPECIALIZATION_COLUMN_IN_TABLE
            + "=" + addQuotationMarkToString("") + " AND "
            + YEAR_OF_SCHOOL_COMPLETION_COLUMN_IN_TABLE
            + "=" + addQuotationMarkToString("") + " AND "
            + ID_COLUMN_IN_TABLE + "=" + addQuotationMarkToNumber(0);
    }
}
