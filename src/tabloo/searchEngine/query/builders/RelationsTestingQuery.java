package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.RelationsTableConfig;

public class RelationsTestingQuery extends SelectQuery {
    private final String TABLE = RelationsTableConfig.TABLE;
    private final String STUDENT_ID_COLUMN_IN_TABLE
        = RelationsTableConfig.STUDENT_ID_COLUMN;
    private final String CLASS_ID_COLUMN_IN_TABLE
        = RelationsTableConfig.CLASS_ID_COLUMN;

    public RelationsTestingQuery() {
        this.query = createBasicSelectFromTable(TABLE);
    }

    public String createTestingQuery() {
        return query + "WHERE " + STUDENT_ID_COLUMN_IN_TABLE
            + "=" + addQuotationMarkToNumber(0) + " AND "
            + CLASS_ID_COLUMN_IN_TABLE + "=" + addQuotationMarkToNumber(0);

    }
}
