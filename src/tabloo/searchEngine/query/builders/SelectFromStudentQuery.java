package tabloo.searchEngine.query.builders;

import tabloo.searchEngine.config.configConteners.StudentTableConfig;

/**
 * Created by getor on 18.03.17.
 */
public class SelectFromStudentQuery extends SelectQuery {
    private final String TABLE = StudentTableConfig.TABLE;
    private final String NAME_COLUMN_IN_TABLE = StudentTableConfig.NAME_COLUMN;
    private final String SURNAME_COLUMN_IN_TABLE = StudentTableConfig.SURNAME_COLUMN;
    private String firstString;
    private String secondString;

    public SelectFromStudentQuery(String firstString, String secondString) {
        this.query = createBasicSelectFromTable(TABLE);
        this.firstString = firstString;
        this.secondString = secondString;
    }

    public SelectFromStudentQuery(int id) {
        super(id);
        this.query = createBasicSelectFromTable(TABLE);
        this.firstString = null;
        this.secondString = null;
    }

    public String createSelectByNameAndSurname() {
        return query + "WHERE " +
            NAME_COLUMN_IN_TABLE + "=" + addQuotationMarkToString(firstString) + " AND " +
            SURNAME_COLUMN_IN_TABLE + "=" + addQuotationMarkToString(secondString);
    }

    public String createSelectBySurnameAndName() {
        return query + "WHERE " +
                NAME_COLUMN_IN_TABLE + "=" +  addQuotationMarkToString(secondString) + " AND " +
                SURNAME_COLUMN_IN_TABLE + "=" + addQuotationMarkToString(firstString);
    }
}
