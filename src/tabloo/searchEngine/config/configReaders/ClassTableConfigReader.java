package tabloo.searchEngine.config.configReaders;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by getor on 06.04.17.
 */
public class ClassTableConfigReader extends ConfigReader{
    private String specializationColumn;
    private String yearOfSchoolCompletionColumn;
    private String urlToFileColumn;

    public ClassTableConfigReader() {
        correctLinesInConfig = 5;
        readConfigFile();
    }

    @Override
    protected void createFileReader() {
        try {
            fileReader = new FileReader("configFiles/ClassTable.config");
        } catch (FileNotFoundException e) {
            System.out.println("Can't open config file");
        }
    }

    @Override
    protected void setValueFromConfigFile(String[] configLine) {
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("TABLE"))
            table = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("ID_COLUMN"))
            idColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("SPECIALIZATION_COLUMN"))
            specializationColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("YEAR_OF_SCHOOL_COMPLETION_COLUMN"))
            yearOfSchoolCompletionColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("URL_TO_FILE_COLUMN"))
            urlToFileColumn = configLine[VALUE_IN_CONFIG];
    }

    @Override
    public String toString() {
        return  super.toString() +"ClassTableConfigReader{" +
                "specializationColumn='" + specializationColumn + '\'' +
                ", yearOfSchoolCompletionColumn='" + yearOfSchoolCompletionColumn + '\'' +
                ", urlToFileColumn='" + urlToFileColumn + '\'' +
                '}';
    }

    public String getSpecializationColumn() {
        return specializationColumn;
    }

    public String getYearOfSchoolCompletionColumn() {
        return yearOfSchoolCompletionColumn;
    }

    public String getUrlToFileColumn() {
        return urlToFileColumn;
    }
}
