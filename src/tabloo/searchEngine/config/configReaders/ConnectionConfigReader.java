package tabloo.searchEngine.config.configReaders;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by getor on 06.04.17.
 */
public class ConnectionConfigReader extends ConfigReader {
    private String ip;
    private String port;
    private String database;
    private String username;
    private String password;

    public ConnectionConfigReader() {
        correctLinesInConfig = 5;
        readConfigFile();
    }

    @Override
    protected void createFileReader() {
        try {
            fileReader = new FileReader("configFiles/Connection.config");
        } catch (FileNotFoundException e) {
            System.out.println("Can't open config file");
        }
    }

    protected void setValueFromConfigFile(String[] configLine) {
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("IP"))
            ip = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("PORT"))
            port = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("DATABASE"))
            database = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("USERNAME"))
            username = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("PASSWORD"))
            password = configLine[VALUE_IN_CONFIG];
    }

    @Override
    public String toString() {
        return "ConnectionConfigReader{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", database='" + database + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
