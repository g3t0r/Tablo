package tabloo.searchEngine.config.configReaders;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by getor on 06.04.17.
 */
abstract class ConfigReader {
    protected final int ARGUMENT_IN_CONFIG = 0;
    protected final int VALUE_IN_CONFIG = 1;
    protected int correctLinesInConfig;
    protected String table;
    protected String idColumn;
    protected FileReader fileReader;
    protected BufferedReader bufferedReader;

    protected void readConfigFile() {
        try {
            readFile();
        } catch (IOException e) {
            System.out.println("Problems while reading the file");
        }
    }

    private void readFile () throws IOException {
        createFileReader();
        bufferedReader = new BufferedReader(fileReader);
        String[] configLine;
        for(int i = 0; i < correctLinesInConfig; i++) {
            configLine = readLineOfFile(bufferedReader);
            if(configLine[0].isEmpty() || isComment(configLine[0])) {
                i--;
                continue;
            }
            setValueFromConfigFile(configLine);
        }
        bufferedReader.close();
    }

    private boolean isComment(String s) {
        String firstLetterOfString = s.substring( 0, 1);
        return firstLetterOfString.equals("#");
    }

    protected abstract void createFileReader();


    private String[] readLineOfFile(BufferedReader bufferedReader) throws IOException {
        String textLine;
        String[] configLine;
        textLine = bufferedReader.readLine();
        textLine = textLine.trim().replaceAll("\\s+", "");
        configLine = textLine.split("=");
        return configLine;
    }

    protected abstract void setValueFromConfigFile(String[] configLine);

    @Override
    public String toString() {
        return "ConfigReader{" +
                "table='" + table + '\'' +
                ", idColumn='" + idColumn + '\'' +
                '}';
    }

    public String getTable() {
        return table;
    }

    public String getIdColumn() {
        return idColumn;
    }
}
