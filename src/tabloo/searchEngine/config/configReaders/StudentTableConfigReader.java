package tabloo.searchEngine.config.configReaders;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by getor on 06.04.17.
 */
public class StudentTableConfigReader extends ConfigReader{
    private String nameColumn;
    private String surnameColumn;

    public StudentTableConfigReader() {
        correctLinesInConfig = 4;
        readConfigFile();
    }

    @Override
    protected void createFileReader() {
        try {
            fileReader = new FileReader("configFiles/StudentTable.config");
        } catch (FileNotFoundException e) {
            System.out.println("Can't open config file");
        }
    }

    @Override
    protected void setValueFromConfigFile(String[] configLine) {
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("TABLE"))
            table = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("NAME_COLUMN"))
            nameColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("SURNAME_COLUMN"))
            surnameColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("ID_COLUMN"))
            idColumn = configLine[VALUE_IN_CONFIG];
    }

    @Override
    public String toString() {
        return super.toString() + "StudentTableConfigReader{" +
                "nameColumn='" + nameColumn + '\'' +
                ", surnameColumn='" + surnameColumn + '\'' +
                '}';
    }

    public String getNameColumn() {
        return nameColumn;
    }

    public String getSurnameColumn() {
        return surnameColumn;
    }
}
