package tabloo.searchEngine.config.configReaders;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by getor on 07.04.17.
 */
public class RelationsTableConfigReader extends ConfigReader{
    private String studentInColumn;
    private String classIdColumn;

    public RelationsTableConfigReader() {
        correctLinesInConfig = 4;
        readConfigFile();
    }

    @Override
    protected void createFileReader() {
        try {
            fileReader = new FileReader("configFiles/RelationsTable.config");
        } catch (FileNotFoundException e) {
            System.out.println("Can't open config file");
        }
    }

    @Override
    protected void setValueFromConfigFile(String[] configLine) {
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("TABLE"))
            table = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("ID_COLUMN"))
            idColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("STUDENT_ID_COLUMN"))
            studentInColumn = configLine[VALUE_IN_CONFIG];
        if(configLine[ARGUMENT_IN_CONFIG].equalsIgnoreCase("CLASS_ID_COLUMN"))
            classIdColumn = configLine[VALUE_IN_CONFIG];
    }

    @Override
    public String toString() {
        return super.toString() + "RelationsTableConfigReader{" +
                "studentInColumn='" + studentInColumn + '\'' +
                ", classIdColumn='" + classIdColumn + '\'' +
                '}';
    }

    public String getStudentIdColumn() {
        return studentInColumn;
    }

    public String getClassIdColumn() {
        return classIdColumn;
    }
}
