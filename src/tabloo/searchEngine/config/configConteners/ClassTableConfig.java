package tabloo.searchEngine.config.configConteners;

/**
 * Created by getor on 26.03.17.
 */
public class ClassTableConfig {
    public static String TABLE;
    public static String ID_COLUMN;
    public static String SPECIALIZATION_COLUMN;
    public static String YEAR_OF_SCHOOL_COMPLETION_COLUMN;
    public static String  URL_TO_FILE_COLUMN ;
}