package tabloo.searchEngine.config.configConteners;

/**
 * Created by getor on 26.03.17.
 */
public class RelationsTableConfig {
    public static String TABLE = "relacje";
    public static String ID_COLUMN = "id";
    public static String STUDENT_ID_COLUMN = "id_ucznia";
    public static String CLASS_ID_COLUMN = "id_rocznika";
}
