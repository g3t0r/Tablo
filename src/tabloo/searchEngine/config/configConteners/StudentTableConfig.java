package tabloo.searchEngine.config.configConteners;

/**
 * Created by getor on 26.03.17.
 */
public class StudentTableConfig {
    public static String TABLE = "uczniowie";
    public static String NAME_COLUMN = "imie";
    public static String SURNAME_COLUMN = "nazwisko";
    public static String ID_COLUMN = "id";
}
