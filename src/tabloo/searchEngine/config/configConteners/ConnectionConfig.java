package tabloo.searchEngine.config.configConteners;

/**
 * Created by getor on 26.03.17.
 */
public class ConnectionConfig {
    public static String IP;
    public static String PORT;
    public static String DATABASE;
    public static String USERNAME;
    public static String PASSWORD;
}
