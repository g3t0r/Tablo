package tabloo.searchEngine.config;

import tabloo.searchEngine.config.configConteners.ClassTableConfig;
import tabloo.searchEngine.config.configConteners.ConnectionConfig;
import tabloo.searchEngine.config.configConteners.RelationsTableConfig;
import tabloo.searchEngine.config.configConteners.StudentTableConfig;
import tabloo.searchEngine.config.configReaders.ClassTableConfigReader;
import tabloo.searchEngine.config.configReaders.ConnectionConfigReader;
import tabloo.searchEngine.config.configReaders.RelationsTableConfigReader;
import tabloo.searchEngine.config.configReaders.StudentTableConfigReader;

/**
 * Created by getor on 07.04.17.
 */
public class ConfigSetter {
    public void setConfigFromConfigFiles() {
        setConnectionConfig();
        setStudentTableConfig();
        setClassTableConfig();
        setRelationsTableConfig();
    }

    public void setConnectionConfig() {
        ConnectionConfigReader config = new ConnectionConfigReader();
        ConnectionConfig.IP = config.getIp();
        ConnectionConfig.PORT = config.getPort();
        ConnectionConfig.DATABASE = config.getDatabase();
        ConnectionConfig.USERNAME = config.getUsername();
        ConnectionConfig.PASSWORD = config.getPassword();
    }

    public void setStudentTableConfig() {
        StudentTableConfigReader config = new StudentTableConfigReader();
        StudentTableConfig.ID_COLUMN = config.getIdColumn();
        StudentTableConfig.TABLE = config.getTable();
        StudentTableConfig.NAME_COLUMN = config.getNameColumn();
        StudentTableConfig.SURNAME_COLUMN = config.getSurnameColumn();
    }

    public void setClassTableConfig() {
        ClassTableConfigReader config = new ClassTableConfigReader();
        ClassTableConfig.TABLE = config.getTable();
        ClassTableConfig.ID_COLUMN = config.getIdColumn();
        ClassTableConfig.SPECIALIZATION_COLUMN = config.getSpecializationColumn();
        ClassTableConfig.YEAR_OF_SCHOOL_COMPLETION_COLUMN = config.getYearOfSchoolCompletionColumn();
        ClassTableConfig.URL_TO_FILE_COLUMN = config.getUrlToFileColumn();
    }

    public void setRelationsTableConfig() {
        RelationsTableConfigReader config = new RelationsTableConfigReader();
        RelationsTableConfig.TABLE = config.getTable();
        RelationsTableConfig.ID_COLUMN = config.getIdColumn();
        RelationsTableConfig.STUDENT_ID_COLUMN = config.getStudentIdColumn();
        RelationsTableConfig.CLASS_ID_COLUMN = config.getClassIdColumn();
    }
}
