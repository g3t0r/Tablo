package tabloo.searchEngine.config.validators;

import tabloo.searchEngine.query.builders.RelationsTestingQuery;

import java.sql.SQLException;
import java.sql.Statement;


public class RelationsSettingsValidator {
    public void testColumnsSettings(Statement statement) throws SQLException {
        RelationsTestingQuery relationsTestingQuery = new RelationsTestingQuery();
        String query = relationsTestingQuery.createTestingQuery();
        statement.executeQuery(query);
    }
}
