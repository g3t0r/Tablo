package tabloo.searchEngine.config.validators;

import tabloo.searchEngine.query.builders.ClassTestingQuery;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by getor on 08.04.17.
 */
public class ClassSettingsValidator {
    public void testColumnsSettings(Statement statement) throws SQLException {
        ClassTestingQuery classTestingQuery = new ClassTestingQuery();
        String query = classTestingQuery.createTestingQuery();
        statement.executeQuery(query);
    }
}
