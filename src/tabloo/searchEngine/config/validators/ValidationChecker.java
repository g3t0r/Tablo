package tabloo.searchEngine.config.validators;

import tabloo.searchEngine.connection.ConnectionBuilder;

import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by getor on 08.04.17.
 */
public class ValidationChecker {
    private Statement statement;

    public boolean testConfigFiles() {
        return
        testConnectionSettings() &&
        testStudentTableSettings() &&
        testClassTableSettings() &&
        testRelationsTableSettings();
    }

    private boolean testConnectionSettings() {
        ConnectionBuilder connectionBuilder = new ConnectionBuilder();
        try {
            statement = connectionBuilder.getConnection().createStatement();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean testStudentTableSettings() {
        StudentSettingsValidator studentSettingsValidator =
                new StudentSettingsValidator();
        try {
            studentSettingsValidator.testColumnsSettings(statement);
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    private boolean testClassTableSettings() {
        ClassSettingsValidator classSettingsValidator =
                new ClassSettingsValidator();
        try {
            classSettingsValidator.testColumnsSettings(statement);
        }catch (SQLException e) {
            return false;
        }
        return true;
    }

    private boolean testRelationsTableSettings() {
        RelationsSettingsValidator relationsSettingsValidator =
                new RelationsSettingsValidator();
        try {
            relationsSettingsValidator.testColumnsSettings(statement);
        } catch (SQLException e) {
            return false;
        }
        return true;
    }



}
