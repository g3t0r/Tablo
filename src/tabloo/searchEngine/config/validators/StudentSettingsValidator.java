package tabloo.searchEngine.config.validators;

import tabloo.searchEngine.query.builders.StudentTestingQuery;

import java.sql.SQLException;
import java.sql.Statement;

public class StudentSettingsValidator {
    public void testColumnsSettings(Statement statement) throws SQLException {
        StudentTestingQuery studentTestingQuery = new StudentTestingQuery();
        String query = studentTestingQuery.createTestingQuery();
        statement.executeQuery(query);
    }
}
