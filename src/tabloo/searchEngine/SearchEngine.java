package tabloo.searchEngine;

import tabloo.searchEngine.result.Result;
import tabloo.searchEngine.search.SearchHandler;
import tabloo.searchEngine.search.SearchRecognizer;
import tabloo.searchEngine.search.StudentSearchHandler;

/**
 * Created by getor on 11.07.17.
 */
public class SearchEngine {
    private String[] phrases;

    public SearchEngine(String[] phrases) {
        this.phrases = phrases;
    }

    public Result[] searchForResults() {
        SearchHandler searchHandler = new SearchRecognizer(phrases).getSearchHandler();
        return searchHandler.getResult();
    }

    public Result[] getAllStudents() {
        return new StudentSearchHandler(new String[]{"","","",""}).getResult();
    }

}
