package tabloo.searchEngine.connection;

import tabloo.searchEngine.config.configConteners.ConnectionConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by getor on 08.03.17.
 */
public class ConnectionBuilder {
    private final String PROTOCOL = "jdbc:mysql";
    private final String IP = ConnectionConfig.IP;
    private final String PORT = ConnectionConfig.PORT;
    private final String DATABASE = ConnectionConfig.DATABASE;
    private final String USERNAME = ConnectionConfig.USERNAME;
    private final String PASSWORD = ConnectionConfig.PASSWORD;

    public Connection getConnection() {
        return createConnection();
    }

    private Connection createConnection() {
        addDriver();
        String url = createUrl();
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void addDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    private String createUrl() {
        return PROTOCOL + "://" + IP + ":" + PORT + "/" + DATABASE;
    }
}
