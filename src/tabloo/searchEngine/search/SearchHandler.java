package tabloo.searchEngine.search;

import tabloo.searchEngine.result.Result;

/**
 * Created by getor on 09.07.17.
 */
public interface SearchHandler {
    public Result[] getResult();
}
