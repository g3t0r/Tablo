package tabloo.searchEngine.search;

import tabloo.searchEngine.config.configConteners.ClassTableConfig;
import tabloo.searchEngine.result.ClassResultHandler;
import tabloo.searchEngine.result.Result;

public class ClassSearchHandler implements SearchHandler{
    private final int SPECIALIZATION = 0;
    private final int YEAR = 1;
    private String query;
    private String specialization;
    private String year;
    private int id = 0;

    public ClassSearchHandler(String[] phrases) {
        specialization = phrases[SPECIALIZATION];
        year = phrases[YEAR];
        query = "SELECT * FROM " + ClassTableConfig.TABLE;

        if(phrasesAreNotEmpty()) {
            query += " WHERE ";
        }
    }

    public ClassSearchHandler(int id) {
        query = "SELECT * FROM " + ClassTableConfig.TABLE;
        this.id = id;
        if(id != 0) query += " WHERE ";
    }

    @Override
    public Result[] getResult() {
        createQuery();
        return new ClassResultHandler(query).getResult();
    }

    public String createQuery() {

        if(id != 0) {
            query += ClassTableConfig.ID_COLUMN + "=" + "\"" + id + "\"";
            return query;
        }

        if(!specialization.isEmpty()) {
            addSpecializationToQuery();
        }

        if(!specialization.isEmpty() && !year.isEmpty()) {
            query += " AND ";
        }

        if(!year.isEmpty()) {
            addYearToQuery();
        }
        return query;
    }

    private void addSpecializationToQuery() {
        query += ClassTableConfig.SPECIALIZATION_COLUMN
                + " LIKE " + "\"%" + specialization + "%\"";
    }

     private void addYearToQuery() {
        query += ClassTableConfig.YEAR_OF_SCHOOL_COMPLETION_COLUMN
                + "=" + year;
     }

    private boolean phrasesAreNotEmpty() {
        return !(specialization.isEmpty() && year.isEmpty());
    }


}
