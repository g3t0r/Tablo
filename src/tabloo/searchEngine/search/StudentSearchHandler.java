package tabloo.searchEngine.search;

import tabloo.searchEngine.config.configConteners.StudentTableConfig;
import tabloo.searchEngine.filtr.StudentResultFilter;
import tabloo.searchEngine.result.Result;
import tabloo.searchEngine.result.StudentResultHandler;

/**
 * Created by getor on 09.07.17.
 */
public class StudentSearchHandler implements SearchHandler{
    private String query;
    private String name;
    private String surname;
    private String specialization;
    private String year;
    public StudentSearchHandler(String[] phrases) {
        name = phrases[0];
        surname = phrases[1];
        specialization = phrases[2];
        year = phrases[3];

        query = "SELECT * FROM " + StudentTableConfig.TABLE;

        if(phrasesAreNotEmpty()) {
            query += " WHERE ";
        }
    }

    public String createQuery() {
        if(!name.isEmpty()) {
            addNameToQuery();
        }

        if(!name.isEmpty() && !surname.isEmpty()) {
            query += " AND ";
        }

        if(!surname.isEmpty()) {
            addSurnameToQuery();
        }
        return query;
    }

    private void addNameToQuery() {
        query += StudentTableConfig.NAME_COLUMN + "=" + "\"" + name + "\"";
    }

    private void addSurnameToQuery() {
        query += StudentTableConfig.SURNAME_COLUMN
                + "=" + "\"" + surname + "\"";
    }

    @Override
    public Result[] getResult() {
        createQuery();
        Result[] results = new StudentResultHandler(query).getArrayOfResults();
        if(!(specialization.isEmpty() && year.isEmpty())) {
            results = new StudentResultFilter(specialization, year, results).getFilteredArray();
        }
        return results;
    }

    private boolean phrasesAreNotEmpty() {
        return !(name.isEmpty() && surname.isEmpty() && surname.isEmpty() && year.isEmpty());
    }
}
