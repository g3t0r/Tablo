package tabloo.searchEngine.search;

import tabloo.searchEngine.result.Result;

public class SearchRecognizer {
    private final int NAME = 0;
    private final int SURNAME = 1;
    private final int SPECIALIZATION = 2;
    private final int YEAR = 3;
    private String name;
    private String surname;
    private String specialization;
    private String year;

    public SearchRecognizer(String[] phrases) {
        name = phrases[NAME];
        surname = phrases[SURNAME];
        specialization = phrases[SPECIALIZATION];
        year = phrases[YEAR];
    }

      public SearchHandler getSearchHandler() {
        if(isSearchAStudentSearch()) {
            return new StudentSearchHandler(new String[]{name, surname, specialization, year});
        }
        return new ClassSearchHandler(new String[]{specialization, year});
      }

      public boolean isSearchAStudentSearch() {
        return !(name.isEmpty() && surname.isEmpty());
      }

}
