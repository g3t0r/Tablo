package tabloo.searchEngine.result;

import tabloo.searchEngine.config.configConteners.ClassTableConfig;
import tabloo.searchEngine.config.configConteners.RelationsTableConfig;
import tabloo.searchEngine.config.configConteners.StudentTableConfig;
import tabloo.searchEngine.connection.ConnectionBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by getor on 01.04.17.
 */
public class ResultHandler {
    protected String query;
    protected int numberOfResults;
    protected ResultSet resultSet;
    private Statement statement;
    protected Result[] arrayForResult;

    public ResultHandler(String query) {
        this.query = query;
        createStatement();
        executeQuery();
        numberOfResults = getNumberOfResults();
        arrayForResult = new Result[numberOfResults];
    }

    protected void createStatement() {
        try {
            this.statement = new ConnectionBuilder().getConnection().createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void executeQuery() {
        try {
            resultSet = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void makeArrayFieldsNotNull(int i) {
        arrayForResult[i] = new Result();
    }

    public int getNumberOfResults() {
        int i = 0;
        try {
            while (resultSet.next()) {
                i++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        setResultSetOnFirstResult();
        return i;
    }

    private void setResultSetOnFirstResult() {
        try {
            resultSet.beforeFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected Result getInfoFromClassTable(Result result) throws SQLException {
        result.classId = resultSet.getInt(ClassTableConfig.ID_COLUMN);
        result.specialization = resultSet.getString(ClassTableConfig.SPECIALIZATION_COLUMN);
        result.yearOfSchoolCompletion = resultSet.getInt(ClassTableConfig.YEAR_OF_SCHOOL_COMPLETION_COLUMN);
        result.urlToFile = resultSet.getString(ClassTableConfig.URL_TO_FILE_COLUMN);
        return result;
    }

    protected Result getInfoFromStudentTable(Result result) throws SQLException {
        result.studentId = resultSet.getInt(StudentTableConfig.ID_COLUMN);
        result.name = resultSet.getString(StudentTableConfig.NAME_COLUMN);
        result.surname = resultSet.getString(StudentTableConfig.SURNAME_COLUMN);
        return result;
    }

    protected Result getInfoFromRelationsTable(Result result) throws SQLException {
        result.relationId = resultSet.getInt(RelationsTableConfig.ID_COLUMN);
        result.classId = resultSet.getInt(RelationsTableConfig.CLASS_ID_COLUMN);
        result.studentId = resultSet.getInt(RelationsTableConfig.STUDENT_ID_COLUMN);
        return result;
    }
}
