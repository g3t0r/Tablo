package tabloo.searchEngine.result;

import java.sql.SQLException;

/**
 * Created by getor on 04.04.17.
 */
public class RelationResultHandler extends ResultHandler{
    public RelationResultHandler(String query) {
        super(query);
    }

    public Result[] getResults() {
        setInformationsFromStudentTableInResults();
        return arrayForResult;
    }

    public void setInformationsFromStudentTableInResults() {
        try {
            int i = 0;
            while (resultSet.next()) {
                makeArrayFieldsNotNull(i);
                arrayForResult[i] = getInfoFromRelationsTable(arrayForResult[i]);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
