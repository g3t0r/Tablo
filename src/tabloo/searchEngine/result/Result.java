package tabloo.searchEngine.result;

/**
 * Created by getor on 25.03.17.
 */
public class Result {
    public String name;
    public String surname;
    public String specialization;
    public String urlToFile;
    public int yearOfSchoolCompletion;
    public int studentId;
    public int classId;
    public int relationId;


    @Override
    public String toString() {
        return "Result{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", specialization='" + specialization + '\'' +
                ", urlToFile='" + urlToFile + '\'' +
                ", studentId=" + studentId +
                ", classId=" + classId +
                ", relationId=" + relationId +
                ", yearOfSchoolCompletion=" + yearOfSchoolCompletion +
                '}';
    }

    public String toStringForButtons() {
        if(name == null || surname == null) return getInfoForClassResult();
        else return getInfoForStudentResult();
    }

    private String getInfoForStudentResult() {
        return name + " " + surname + " " + getInfoForClassResult();

    }

    private String getInfoForClassResult() {
        return specialization + " " + yearOfSchoolCompletion;
    }
}
