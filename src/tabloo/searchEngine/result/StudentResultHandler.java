package tabloo.searchEngine.result;

import tabloo.searchEngine.query.builders.SelectFromClassesQuery;
import tabloo.searchEngine.query.builders.SelectFromRelationsQuery;

import java.sql.SQLException;

/**
 * Created by getor on 25.03.17.
 */
public class StudentResultHandler extends ResultHandler {
    public StudentResultHandler(String query) {
        super(query);
    }

    public Result[] getArrayOfResults() {
        try {
            setInformationFromStudentTableInResults();
            setInformationFromRelationsTableInResults();
            setInformationFromClassTableInResults();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayForResult;
    }

    private void setInformationFromStudentTableInResults() throws SQLException {
        executeQuery();
        int i = 0;
        while(resultSet.next()){
            makeArrayFieldsNotNull(i);
            arrayForResult[i] = getInfoFromStudentTable(arrayForResult[i]);
            i++;
        }
    }

    private void setInformationFromClassTableInResults() throws SQLException {
        for(int i = 0; i < numberOfResults; i++) {
            prepareSelectFromClassesQuery(i);
            executeQuery();
            if(resultSet.next()) {
               arrayForResult[i] = getInfoFromClassTable(arrayForResult[i]);
            }
        }
    }

    private void setInformationFromRelationsTableInResults() throws SQLException {
        for(int i = 0; i < numberOfResults; i++) {
            prepareSelectFromRelationsQuery(i);
            executeQuery();
            if(resultSet.next()) {
                arrayForResult[i] = getInfoFromRelationsTable(arrayForResult[i]);
            }
        }
    }

    private void prepareSelectFromClassesQuery(int i) {
        this.query = new SelectFromClassesQuery(arrayForResult[i].classId)
                .createSelectById();
    }

    private void prepareSelectFromRelationsQuery(int i) {
        this.query = new SelectFromRelationsQuery(arrayForResult[i].studentId)
                .createSelectByStudentId();
    }
}
