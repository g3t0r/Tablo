package tabloo.searchEngine.result;

import java.sql.SQLException;

/**
 * Created by getor on 01.04.17.
 */
public class ClassResultHandler extends ResultHandler{
    public ClassResultHandler(String query) {
        super(query);
    }

    public Result[] getResult() {
        return getArrayOfResults();
    }

    private Result[] getArrayOfResults() {
        try {
            int i = 0;
            while(resultSet.next()) {
                makeArrayFieldsNotNull(i);
                arrayForResult[i] = getInfoFromClassTable(arrayForResult[i]);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayForResult;
    }
}
